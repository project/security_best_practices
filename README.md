CONTENTS OF THIS FILE
----------------------
 
  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers
 
 
INTRODUCTION
------------
 
Security Best Practice is a module that audits a drupal project in terms of security parameters. 
It evaluates the security of headers, password policies helping to prevent clickjacking, brute-force attacks.
 
 
REQUIREMENTS
------------
 
This module usage implies the installation of module https://www.drupal.org/project/phpspreadsheet 
 
 
INSTALLATION
------------
 
  1. Copy security_best_practices folder to modules directory
  2. At Administration >> Extend, enable the Security Best Practices module
 
 
CONFIGURATION
-------------
 
There is no configuration page in this release
 
 
MAINTAINERS
-----------

Maintainer: 
icardoss(https://www.drupal.org/u/icardoss)
miguelpamferreira (https://www.drupal.org/u/miguelpamferreira)
bcruzcar(https://www.drupal.org/u/bcruzcar)
sara_asb(https://www.drupal.org/u/sara_asb)
